# Front-End Interview Test

----
## Getting started on macOS
This application was bootstrapped with [create react app](https://github.com/facebookincubator/create-react-app) have a look at the documentation for any further information about the application setup.

Install [Brew](http://brew.sh)

Install [node](https://changelog.com/posts/install-node-js-with-homebrew-on-os-x)
```
brew install node
```

clone the repository

```
git clone https://bitbucket.org/lelith/game_of_threes.git
cd game_of_threes
```

Start the application
```
npm install
npm start
```
Runs the app in development mode.
Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.
You will see the build errors and lint warnings in the console

## Testing
Run the tests
```
npm test
```
For further information read in the [create react app documentation] (https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#running-tests)

## Sass/SCSS Linter
This project is using [csscomb](http://csscomb.com/docs/usage-node)
You can install csscomb in your [atom.io](https://atom.io/packages/atom-csscomb) editor

## Used asset resources
Icons made by [Google](http://www.flaticon.com/authors/google) from [www.flaticon.com](http://www.flaticon.com) is licensed [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)
