import React, { Component } from "react";
import "./Avatar.css"
import avatarImg from "../../../assets/images/user-shape.svg"
import avatarImgFallback from "../../../assets/images/user-shape.png"
var Isvg = require("react-inlinesvg");

class Avatar extends Component {
  render() {
    return (
      <figure className="avatar-left">
        <Isvg src={avatarImg}>
          <img src={avatarImgFallback} />
        </Isvg>
      </figure>
    );
  }
}

export default Avatar
