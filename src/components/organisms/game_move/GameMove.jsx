import React, { Component } from "react";
import "./GameMove.scss";
import Avatar from "../../molecules/avatar/Avatar"

class GameMove extends Component {
  constructor(props) {
    super(props);
    this.handleOptionChange = this.handleOptionChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleOptionChange (event){
    let action = event.target.value;
  }

  handleSubmit(event) {
    event.preventDefault();
    let number = this.number;
    let action = this.action;
    this.calculateMove(number, action);
  }

  calculateMove(number, action){
    alert("number: " + number.value + "action: "+ action.value)

    switch (action.value) {
      case "add":
          number.value +=1;
        break;
      case "sub":
          number.value -=1;
        break;
    }

    alert("number: " + number.value + "action: "+ action.value)

    number.value = Math.floor(number.value/3)
    alert("new number is " + number.value);
    return number.value
  }

  render() {
    return (
      <section className="gamemove">
        <Avatar />
        <form onSubmit={this.handleSubmit}>
          <input type="number" name="number" ref={(input) => this.number = input} />
          <input type="radio" id="p1" name="action" value="add"  ref={(input) => this.action = input} onChange={this.handleOptionChange}/>
          <label htmlFor="p1">+ 1</label>

          <input type="radio" id="neutral" name="action" value="neutral" ref={(input) => this.action = input} onChange={this.handleOptionChange} />
          <label htmlFor="neutral">0</label>

          <input type="radio" id="m1" name="action" value="sub" ref={(input) => this.action = input}  onChange={this.handleOptionChange} />
          <label htmlFor="m1">- 1</label>

          <input type="submit" value="Submit" />
        </form>
      </section>
    );
  }
}

export default GameMove
