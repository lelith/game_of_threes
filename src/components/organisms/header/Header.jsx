import React, { Component } from "react";
import "./Header.css";

class Header extends Component {
  render() {
    return(
      <header className="header">
        <h1 className="header-headline">
          <span className="content">Game of Threes</span>
        </h1>
      </header>
    );
  }
}

export default Header
