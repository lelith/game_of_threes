import React, { Component } from "react";
import {
  Link
} from 'react-router-dom'
import "./Menu.css";

class Menu extends Component {
  render() {
    return(
      <section className="menu">
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/game">Game</Link></li>
        </ul>
      </section>
    );
  }
}

export default Menu
