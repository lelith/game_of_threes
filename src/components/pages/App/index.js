import React from 'react';
import {render} from 'react-dom';
import Header from '../../organisms/header/Header';
import Menu from '../../organisms/menu/Menu';


const App = () => (
  <div>
    <Header />
    <Menu />
  </div>
)

export default App;
