import React, { Component } from "react";
import GameMove from "../../organisms/game_move/GameMove"

class Game extends Component {
  render(){
    return (
      <section className="game">
        <GameMove />
      </section>
    );
  }
}


export default Game
