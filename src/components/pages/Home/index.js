import React from 'react'
import {render} from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

const Home = () => (
 <section className="home">
 <h2 className="headline-20">Rules</h2>
  <ul className="rules">
    <li>Join an ongoing game or create a new one</li>
    <li>Every move you have three options. Either add 1 to the number, substract 1 from the number or pass</li>
    <li>The number gets then divided by 3. If the solution is 1 you have won. Otherwise the other player is on the move.</li>
  </ul>
 </section>
)

export default Home;
