import React from 'react'
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'

import Home from './components/pages/Home';
import App from './components/pages/App';
import Game from './components/pages/Game';
import './index.css';

ReactDOM.render(
    (
      <Router>
        <div className="grid">
          <Route exact path="/" component={App}/>
          <Route exact path="/" component={Home}/>
          <Route path="/game" component={App}/>
          <Route path="/game" component={Game}/>
        </div>
      </Router>
  ),document.getElementById('root')
);

registerServiceWorker();
